import './style.css'
import * as THREE from 'three'
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js'
// import * as dat from 'dat.gui'
import { RoomEnvironment } from 'three/examples/jsm/environments/RoomEnvironment.js';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {DRACOLoader} from "three/examples/jsm/loaders/DRACOLoader";
import gsap, {Power2, Power4, Back} from 'gsap/all'

// Debug
// const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Objects
const loader = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath('/examples/js/libs/draco/');
loader.setDRACOLoader(dracoLoader);

loader.load(
    './models/ash/scene.gltf',
    function (gltf) {
        const object = gltf.scene
        const box = new THREE.Box3().setFromObject( object );
        const center = box.getCenter( new THREE.Vector3() );

        object.position.x += ( object.position.x - center.x );
        object.position.y += ( object.position.y - center.y );
        object.position.z += ( object.position.z - center.z );

        scene.add(object)

        gsap.to('.overlay', {
            yPercent: 100,
            duration: 1.6,
            ease: Power2
        })

        gsap.to( camera.position, {
            duration: 3,
            delay: 0.4,
            ease: Back.easeOut.config(1.2),
            x: 0,
            y: 0,
            z: 1.2,
        } );
    },
    // called while loading is progressing
    function (xhr) {
        console.log((xhr.loaded / xhr.total * 100) + '% loaded');
    },
    // called when loading has errors
    function (error) {
        console.log('An error happened');
    }
)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0.4318820711024913
camera.position.y = -0.7996932419290741
camera.position.z = 0.353943691135352

camera.rotation.x = 1.1541380104600425
camera.rotation.y = 0.4586693593425618
camera.rotation.z = -0.7856071173650925
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

// controls.addEventListener( "change", event => {
//     console.log( controls.object.position );
//     console.log( controls.object.rotation );
// })

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    alpha: true,
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const pmremGenerator = new THREE.PMREMGenerator( renderer );
scene.environment = pmremGenerator.fromScene( new RoomEnvironment(), 0.04 ).texture;
/**
 * Animate
 */

const clock = new THREE.Clock()

const tick = () => {

    const elapsedTime = clock.getElapsedTime()

    // Update Orbital Controls
    controls.update()
    // console.log(controls)

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()